# something
class Attendee
  ATTRS = %w[id reg_date first_name last_name email_address home_phone street city state zipcode].freeze
  ATTRS.each { |attr| attr_accessor attr.to_sym }

  def initialize(args)
    attributes = args.split(',')
    ATTRS.each_with_index { |attr, i| __send__("#{attr}=", attributes[i].chomp) }
  end

  def zipcode
    @zipcode.to_s.rjust(5, '0')[0..4]
  end
end
