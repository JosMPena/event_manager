require 'spec_helper'
require 'attendee'
require 'csv'

describe Attendee do
  attrs = CSV.foreach(
    File.join(Dir.home, 'Projects/event_manager/data/small_sample.csv'), headers: true
  ).take(1).first.to_s

  let(:attendee) { Attendee.new(attrs) }

  describe 'instance values' do
    it { expect(attendee.first_name).must_equal 'Allison' }
    it { expect(attendee.zipcode).must_equal '20010' }
    it 'should auto-correct wrong zipcode' do
      # if the zip code is more than 5 digits, truncate it to the first 5 digits
      attendee.zipcode = '1234567'
      expect(attendee.zipcode.size).must_equal 5
      # if the zip code is less than 5 digits, add zeros to the front until it becomes five digits
      attendee.zipcode = '66'
      expect(attendee.zipcode.size).must_equal 5
      # if the zip code is empty, assume a default value of '00000'
      attendee.zipcode = nil
      expect(attendee.zipcode.size).must_equal 5
      expect(attendee.zipcode).must_equal '00000'
    end
  end
end
